var Todo = require('./models/todo.js')
// routes ======================================================================

function getTodos (res) {
        // get and return all the todos after you create another
        Todo.find(function(err, todos) {
            if (err)
                res.send(err)
            res.json(todos);
        });
    }

    module.exports = function (app) {
    // api ---------------------------------------------------------------------
    // get all todos
    app.get('/api/todos', function(req, res) {
        getTodos(res);
    });

    // create todo and send back all todos after creation
    app.post('/api/todos', function(req, res) {

        // create a todo, information comes from AJAX request from Angular
        Todo.create({
            text : req.body.text,
            done : false,
            added: req.body.added
        }, function(err, todo) {
            if (err)
                res.send(err);
            getTodos(res);
        });

    });

    // delete a todo
    app.delete('/api/todos/:todo_id', function(req, res) {
        Todo.remove({
            _id : req.params.todo_id
        }, function(err, todo) {
            if (err)
                res.send(err);

            getTodos(res);
        });
    });

// application -------------------------------------------------------------

var config = require('./config/config')
app.get('/', function(req, res) {
        res.sendfile(config.homePage()); // load the single view file (angular will handle the page changes on the front-end)
    });
}