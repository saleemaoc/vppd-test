module.exports = {
    debug: function() {
        var os = require("os");
        return os.hostname() === 'suserver';
    },
    dbUrl : function() {
        return this.debug() ? 'mongodb://localhost:27017/vppddb' : 'mongodb://waffle.modulusmongo.net:27017/a7qyHobu';
        // 'mongodb://$OPENSHIFT_MONGODB_DB_HOST:$OPENSHIFT_MONGODB_DB_PORT/';
    },
    homePage: function() {
        return this.debug() ? './app/index.html' : './dist/index.html';
    }
};
