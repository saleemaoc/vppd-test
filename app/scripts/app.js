'use strict';

/**
 * @ngdoc overview
 * @name vppdApp
 * @description
 * # vppdApp
 *
 * Main module of the application.
 */
angular
  .module('vppdApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ui.bootstrap',
    'ui.bootstrap.datetimepicker'
  ])
  .controller('HeaderCtrl', function ($scope, $location) { 
    $scope.isActive = function (viewLocation) { 
      // console.log(viewLocation);
      // console.log($location.path)
      return viewLocation === $location.path();
    };
  })
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/task1.html',
        controller: 'Task1Ctrl',
        controllerAs: 'main'
      })
      .when('/task2', {
        templateUrl: 'views/task2.html',
        controller: 'Task2Ctrl',
        controllerAs: 'task2'
      })
      .when('/task3', {
        templateUrl: 'views/task3.html',
        controller: 'Task3Ctrl',
        controllerAs: 'task3'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
