'use strict';

/**
 * @ngdoc function
 * @name vppdApp.controller:Task2Ctrl
 * @description
 * # Task2Ctrl
 * Controller of the vppdApp
 */
 angular.module('vppdApp')
 .controller('Task2Ctrl', function ($scope, $http) {

    $scope.formData = {};
    $scope.formData.added = "";
    $scope.loading = true;

    $scope.isOpen = false;

    $scope.openCalendar = function(e) {
      console.log('opening openCalendar')
        e.preventDefault();
        e.stopPropagation();
        $scope.isOpen = true;
    };

    // when landing on the page, get all todos and show them
    $http.get('/api/todos')
    .success(function(data) {
      $scope.todos = data;
      console.log(data);
      $scope.loading = false;
    })
    .error(function(data) {
      console.log('Error: ' + data);
    });

// when submitting the add form, send the text to the node API
$scope.createTodo = function() {
  console.log($scope.formData.added);
  if($scope.formData.text != undefined) {
    $scope.loading = true;
    $http.post('/api/todos', $scope.formData)
    .success(function(data) {
                  $scope.formData = {}; // clear the form so our user is ready to enter another
                  $scope.todos = data;
                  // console.log(data);
                  $scope.loading = false;
                })
    .error(function(data) {
      console.log('Error: ' + data);
    });
  }
};

    // delete a todo after checking it
    $scope.deleteTodo = function(id) {
      $scope.loading = true;
      $http.delete('/api/todos/' + id)
      .success(function(data) {
        $scope.todos = data;
        // console.log(data);
        $scope.loading = false;
      })
      .error(function(data) {
        console.log('Error: ' + data);
      });
    };
  });
