'use strict';

function drawD3Chart (data) {

  var w = 40, h = 300, barGap = 5, animDur = 500;

  var margin = {top: 20, right: 30, bottom: 30, left: 40};
  var width = (w + barGap)* data.length + margin.left + margin.right;
  var height = 300 + margin.bottom + margin.top;

  d3.select(".chart").selectAll("svg").remove();

  var chart = d3.select(".chart").append("svg").attr("class", "chart").attr("width", width).attr("height", height);

  var x = d3.scale.linear().domain([0, 1]).range([0, (w+barGap)]);

  var y = d3.scale.linear().domain([0, d3.max(data)]).rangeRound([0, h]); //rangeRound is used for antialiasing

  chart.selectAll("rect")
  .data(data)
  .enter().append("rect")
  .style("stroke", "#fff")
  // x and y are the lower-left position of the bar
  .attr("x",0)
  .attr("y",h)
  .attr("x", function(d, i) { return (x(i) - .5) + margin.left + barGap; }) // for crisp edges use -.5 (antialiasing)
  .attr("y", function(d) { return h - y(d) + margin.top - .5; })
  // width is the width of the bar
  .attr("width", w)
  // height is the height of the bar
  .attr("height", 0)
  .transition().attr("height", function(d) { return y(d);}).duration(animDur)


  var xAxisScale = d3.scale.ordinal()
      .domain(d3.range(0, data.length))
      .rangeRoundBands([0, width - margin.left - margin.right + barGap], .1);
  
  var xAxis = d3.svg.axis().scale(xAxisScale).orient("bottom");

  chart.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(" + margin.left + ", " + (h+margin.top) + ")")
        .call(xAxis);

  var yAxisScale = d3.scale.linear().domain([d3.max(data), 0]).range([0, height - margin.bottom - margin.top]);
  var yAxis = d3.svg.axis().scale(yAxisScale).orient("left").ticks(10);

  chart.append("g")
    .attr("class", "y axis")
    .attr("transform", "translate(" + margin.left + ", " + margin.top + ")")
    .call(yAxis)
}

/**
 * @ngdoc function
 * @name vppdApp.controller:Task3Ctrl
 * @description
 * # Task3Ctrl
 * Controller of the vppdApp
 */
 angular.module('vppdApp')
 .controller('Task3Ctrl', function ($scope) {

   var data = [10, 8, 100, 16, 23, 50];

   $scope.data = data;

   $scope.drawChart = function (d) {
    if(d.constructor === String) {
      console.log(d)
      d = d.split(",");
      var s = []
      d.forEach(function(i){
        s.push(+i);
      })
      d = s;
    }
    console.log(d)
    $scope.data = d;
    drawD3Chart($scope.data);
  }

$scope.drawChart(data); // draw chart on load
});



/*
    var x = d3.scaleLinear()
    .domain([0, d3.max(data)])
    .range([0, 420]);

    d3.select(".chart")
    .selectAll("div")
    .data(data)
    .enter()
    .append("div")
    .style("width", function(d) { return x(d) + "px"; })
    .text(function(d) { return d; });
    */

/*var margin = {top: 20, right: 20, bottom: 70, left: 40},
    width = 600 - margin.left - margin.right,
    height = 300 - margin.top - margin.bottom;

// Parse the date / time
var parseDate = d3.time.format("%Y-%m").parse;

var x = d3.scale.ordinal().rangeRoundBands([0, width], .05);
var y = d3.scale.linear().range([height, 0]);

var xAxis = d3.svg.axis()
    .scale(x)
    .orient("bottom")
    .tickFormat(d3.time.format("%Y-%m"));

var yAxis = d3.svg.axis()
    .scale(y)
    .orient("left")
    .ticks(10);

var svg = d3.select("body").append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
  .append("g")
    .attr("transform", 
          "translate(" + margin.left + "," + margin.top + ")");

d3.csv("bar-data.csv", function(error, data) {

    data.forEach(function(d) {
        d.date = parseDate(d.date);
        d.value = +d.value;
    });
 
  x.domain(data.map(function(d) { return d.date; }));
  y.domain([0, d3.max(data, function(d) { return d.value; })]);

  svg.append("g")
      .attr("class", "x axis")
      .attr("transform", "translate(0," + height + ")")
      .call(xAxis)
    .selectAll("text")
      .style("text-anchor", "end")
      .attr("dx", "-.8em")
      .attr("dy", "-.55em")
      .attr("transform", "rotate(-90)" );

  svg.append("g")
      .attr("class", "y axis")
      .call(yAxis)
    .append("text")
      .attr("transform", "rotate(-90)")
      .attr("y", 6)
      .attr("dy", ".71em")
      .style("text-anchor", "end")
      .text("Value ($)");

  svg.selectAll("bar")
      .data(data)
    .enter().append("rect")
      .style("fill", "steelblue")
      .attr("x", function(d) { return x(d.date); })
      .attr("width", x.rangeBand())
      .attr("y", function(d) { return y(d.value); })
      .attr("height", function(d) { return height - y(d.value); });

  });*/